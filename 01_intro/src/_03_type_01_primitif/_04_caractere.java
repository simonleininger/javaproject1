package _03_type_01_primitif;

public class _04_caractere {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char laDerniere = 'a';
		char la3eme = 'é';
		char la4eme = 'i';
		char laDeux = 'm';
		char manque = 'l';
		/*
		 *\n retour à la ligne
		 *\t tabulation
		 *\\ anti-slash
		 *\' apostrophe
		 */
		boolean isDigit=Character.isDigit(la4eme);
		System.out.printf("%c%c%c%c%c%c\n",Character.toUpperCase(laDerniere),laDeux,la3eme,manque,la4eme,laDerniere);
		System.out.println(isDigit);
	}
}