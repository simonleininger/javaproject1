package _03_type_01_primitif;

public class _02_entier {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte entierByte = 20;
		short entierShort = 20_000;
		int entierInt = 2_000_000_000;
		
		//toujours rajouter un L à la fin d'un nombre de type long
		long entierLong = 2_000_000_000_000_000_000L; 
		
		//à chaque type primitif est associé un type complexe
		//proposant des propriétés et des méthodes utilitaire
		
		/*
		 * type primitif || type complexe
		 * byte          || Byte
		 * short         || Short
		 * int           || Integer
		 * long          || Long
		 * float         || Float
		 * double        || Double
		 * char          || Char
		 * boolean       || Boolean
		 */
		
		System.out.printf(" - %s (%d bits) from %d to %d\n", Byte.TYPE, Byte.SIZE, Byte.MIN_VALUE, Byte.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %d to %d\n", Short.TYPE, Short.SIZE, Short.MIN_VALUE, Short.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %d to %d\n", Integer.TYPE, Integer.SIZE, Integer.MIN_VALUE, Integer.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %d to %d\n", Long.TYPE, Long.SIZE, Long.MIN_VALUE, Long.MAX_VALUE);
		System.out.println("- "+entierByte+" -"+entierShort+" -"+entierInt+" -"+entierLong);
	}

}
