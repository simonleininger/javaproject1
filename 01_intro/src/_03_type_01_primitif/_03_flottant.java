package _03_type_01_primitif;

public class _03_flottant {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float monFloat = 28.10F;
		double monDouble = 20.22;
		
		System.out.printf(" - %s (%d bits) from %e to %e\n", Float.TYPE, Float.SIZE, Float.MIN_VALUE, Float.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %e to %e\n", Double.TYPE, Double.SIZE, Double.MIN_VALUE, Double.MAX_VALUE);
		System.out.println(monFloat+monDouble);
		System.out.println(Double.POSITIVE_INFINITY);
		System.out.println(Double.NEGATIVE_INFINITY);
		System.out.println(Double.NaN); // Nan : Not a number
		
		System.out.println(Float.isFinite(Float.NEGATIVE_INFINITY));
	}

}
