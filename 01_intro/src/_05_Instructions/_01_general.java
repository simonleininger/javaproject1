package _05_Instructions;

public class _01_general {

	/**
	 * Une instruction simple se termine par un point virgule; un bloque
	 * d'instruction est contenu entre acolade {} Les variables déclarés dans un
	 * bloc d'instruction ne sont accessible uniquement dans ce bloc
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Cette instruction se termine par un \";\"");
		{
			System.out.println("Ceci est la première instruction d'un bloc entouré par \"{ }\" ");
			int a = 10;
			System.out.println("Ceci est la dernière instruction du bloc, avec la création de la variable a = " + a
					+ " qui n'existera plus après.");
		}
		System.out.println("a n'existe plus");
	}

}
