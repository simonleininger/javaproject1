package _05_Instructions;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class _03_Boucle {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		/*
		 * for (à partir de, tant que, incrémentation)
		 */
		Scanner clavier = new Scanner(System.in);
		String[] simpson = { "Homer", "Marge", "Bart", "Lisa", "Maggie" };

		System.out.println("--------------------------------- BOUCLE FOR ---------------------------------");
		for (int index = 0; index < simpson.length; index++) {
			System.out.println(simpson[index]);
		}
		System.out.println("---------------------------- BOUCLE FOR AVEC BREAK ---------------------------");
		for (int index = 0; index < simpson.length; index++) {
			if (simpson[index] == "Bart") {
				System.out.println("L'instruction 'break' permet de stoper la boucle");
				break;
			}
			System.out.println(simpson[index]);
		}
		System.out.println("-------------------------- BOUCLE FOR AVEC CONTINUE --------------------------");
		for (int index = 0; index < simpson.length; index++) {
			if (simpson[index] == "Bart") {
				System.out.println("L'instruction 'continue' permet de passer à l'itération suivante (plus de Bart)");
				continue;
			}
			System.out.println(simpson[index]);
		}
		/*
		 * While (condition) TANT QUE la condition est vrai la boucle continue
		 */
		System.out.println("-------------------------------- BOUCLE WHILE --------------------------------");
		System.out.println("Entrez votre age");
		int age = clavier.nextInt();
		while (age <= 0) {
			System.out.println("Entrez un age positif");
			age = clavier.nextInt();
		}
		System.out.println("Vous avez " + age + " ans.");
		
		/*
		 * La boucle DO While se différencie de la boucle while en ce que la condition est vérifié après l'exécution
		 * du bloc d'instruction 
		 * on est donc certain d'exécuter au moins une fois l'exécution
		 */
		age = 0;
		System.out.println("------------------------------ BOUCLE DO WHILE -------------------------------");
		do {
			System.out.println("Entrez l'age de votre Belle-mère");
			age = clavier.nextInt();
		}while (age <= 0);
		System.out.println("Encore "+(120-age)+" ans à la supporter, bonne chance");

		
		System.out.println("---------------------------- BOUCLE NANANANANERE -----------------------------");
		for (int i = 0; i < 1000; i++) { 
			System.out.print("V");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("o");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("u");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("s");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print(" ");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("n");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("'");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("a");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("v");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("e");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("z");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print(" ");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("p");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("a");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("s");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print(" ");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("d");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("i");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("t");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print(" ");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("S");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("V");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print("P");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print(".");
			TimeUnit.MILLISECONDS.sleep(10);
			System.out.print(" ");
			TimeUnit.MILLISECONDS.sleep(10);
			
		}
		System.out.println();
		
		clavier.close();
	}

}
