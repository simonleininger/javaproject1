package _05_Instructions;

import java.util.Scanner;

public class _02_Conditionelles {
	public static void main(String[] args) {
		Scanner clavier = new Scanner(System.in);

		System.out.println("Entrez votre age svp : ");
		int n = clavier.nextInt();

		if (n < 25) {
			System.out.println("Vous êtes jeune.");
		} else if (n < 50) {
			System.out.println("Vous êtes vieux.");
		} else {
			System.out.println("Vous êtes bientôt arriver.");
		}

		System.out.println("Entrez le jour de la semaine : ");
		int jour = clavier.nextInt();

		switch (jour) {
		case 1:
			System.out.println("Lundi");
			break;
		case 2:
			System.out.println("Mardi");
			break;
		case 3:
			System.out.println("Mercredi");
			break;
		case 4:
			System.out.println("Jeudi");
			break;
		case 5:
			System.out.println("Vendredi");
			break;
		case 6:
			System.out.println("Samedi");
			break;
		case 7:
			System.out.println("Dimanche");
			break;
		default:
			System.out.println("Entre 1 et 7 G**************");
			break;
		}

		switch (jour) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			System.out.println("Vas bosser C******");
			break;
		case 6:
		case 7:
			System.out.println("C'EST LE WEEKEND");
			break;
		}

		clavier.close();
	}
}
