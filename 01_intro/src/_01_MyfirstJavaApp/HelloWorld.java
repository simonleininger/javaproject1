package _01_MyfirstJavaApp;

/**
 * Commentaire de documentation : Première Application Java
 * @author Simon
 *
 */
public class HelloWorld {
/**
 * 
 * @param args
 */
	public static void main(String[] args) {
		//affiche Hello World
		/*
		 * commentaire sur plusieurs ligne
		 */
		System.out.println("Hello World !!!");
	}

}
