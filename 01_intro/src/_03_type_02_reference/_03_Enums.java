package _03_type_02_reference;

/**
 * 
 * Un type énuméré est un type de données comportant un ensemble fini d'états auxquels sont associés un nom
 * un type énuméré doit être déclaré dans un fichier séparé portant le nom de l'énum
 * 
 * @author Simo
 *
 */
public class _03_Enums {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MonEnum rouge = MonEnum.ROUGE;
		System.out.println(rouge);
	}

}
