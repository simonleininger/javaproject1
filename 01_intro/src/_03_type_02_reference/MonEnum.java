package _03_type_02_reference;

/**
 * 
 * Un type énuméré est un type de données comportant un ensemble fini d'états auxquels sont associés un nom
 * un type énuméré doit être déclaré dans un fichier séparé portant le nom de l'énum
 * Un type énuméré s'introduit avec le mot clé enum
 * Conventionnelement :
 *  Le nom commence par une majuscule
 *  les nom d'état sont en MAJUSCULE
 *  et utilisation de _ pour les mots composés
 * 
 * @author Simon
 *
 */ 

public enum MonEnum {
	VERT, ORANGE, ROUGE, ROUGE_FONCE
}
