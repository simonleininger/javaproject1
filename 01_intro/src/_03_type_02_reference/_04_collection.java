package _03_type_02_reference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**+
 * 
 * @author Simon
 *
 */
public class _04_collection {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<String>();
		
		list.add("B");
		list.add("i");
		list.add("b");
		list.add("i");
		list.add("b");
		list.add("o");
		list.add("b");
		list.add("i");
		list.add("d");
		list.add("i");
		list.add("b");
		list.add("o");
		list.add("u");
		
		for (int i=0; i<list.size(); i++) {
			System.out.print(list.get(i));
		}
		System.out.println();
		for (String str:list) {
			System.out.print(str);
		}
		System.out.println();	
		/*
		 * Méthode des collections
		 */
		
		System.out.println(list+" (Liste) ");	
		Collections.rotate(list, 1);
		System.out.println(list+" (Rotation 1) ");
		Collections.rotate(list, -1);
		Collections.rotate(list, -1);
		System.out.println(list+" (Rotation -1) ");
		Collections.shuffle(list);
		Collections.sort(list);
		System.out.println(list+" (Trier) ");
		Collections.reverse(list);
		System.out.println(list+" (Reverse) ");
		System.out.println(list+" (Shuffle) ");
		Collections.shuffle(list);
		System.out.println(list+" (Shuffle) ");
		List<String> sousListe = list.subList(2, 5);
		System.out.println(sousListe+" (Sousliste 2 à 5) ");
			
		/*
		 * Tableau associatif parfois appelé dictionnaire ou map
		 * permet d'associer une clé à une valeur (un mot à une définition)
		 * un tableau assosiatif ne peut pas contenir de doublon de clé.	
		 */
		
		Map<Integer, String> map = new TreeMap<Integer, String>();
		// Map<Integer, String> map = new HashMap<Integer, String>();
		// Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		map.put(1, "Lundi");
		map.put(2, "Mardi");
		map.put(6, "Samedi");
		map.put(3, "Mercredi");
		map.put(4, "Jeudredi");
		map.put(5, "Vendredi");
		map.put(7, "Dimanche");
		map.put(8, "Merdredi");
		map.put(4, "Jeudi"); // la clé existe déjà elle ne sera pas ajouté mais remplacera la valeur
		
		for (Integer item : map.keySet()) {
			
			System.out.println(item+ " : " + map.get(item));
			
		}
		
		map.remove(8);
		
		for (Map.Entry<Integer, String> entry : map.entrySet()) {
			
			Integer key = entry.getKey();
			String value = entry.getValue();
			System.out.println("Clé : "+key+" - Valeur : "+value);
		}
		
		
		
		for (int i=0; i<10000; i++) {
			System.out.print("ha");
			TimeUnit.MILLISECONDS.sleep(50);
		}
	}
}
