package _03_type_02_reference;

import java.util.ArrayList;

public class _02_tableau {
/**
 * un Tableau est de type complexe 
 * une fois initialisé, il n'est plus possible de changé sa taille
 * Mais comme pour la classe String, il est néanmoins possible de réaffecté un nouveau tableau à la place de l'ancien
 * @param args
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] tab = {1,2,3,5,8,13}; //déclaration d'un tableau d'entier non initialisé.
		String[] names= {"riri", "fifi", "loulou"}; //Déclaration et initialisation d'un tableau de chaine de caractère
		System.out.println(tab.length);
		System.out.println(names.length);
		System.out.println("Parcours du tableau avec une boucle \"for\" traditionnel");
		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}
		
		System.out.println("Parcours du tableau avec une boucle \"foreach\"");
		for (String name :names) {
			System.out.println(name);
		} 
		
		/*
		 * Tableau de Tableaux
		 */
		
		String [][] names2D = {{"Riri", "Fifi", "Loulou"},{"Tic","Tac"}};
		
		for (int line=0; line < names2D.length; line++) {
			for (int column=0; column < names2D[line].length; column++) {
				System.out.print(names2D[line][column] + " ");
			}
			System.out.println();
		}
 		
		/*
		 * ArrayList
		 * 
		 * un ArrayList est un objet de type collection, comme un tableau
		 * une collection permet de stocker un ensemble de valeur
		 * mais contrairement à un tableau, un ArrayList a une taille dynamique
		 * 
		 */
		
		ArrayList<String> collection= new ArrayList<String>();
		
		collection.add("Bob");
		collection.add("Patrick");
		collection.add("Carlos");
		collection.add("Captain Krabs");
		collection.add("Plankton");
		collection.add("Gary");
		collection.add("Sandy");
		
		System.out.println("Size = " + collection.size());
			
		
	}

}
