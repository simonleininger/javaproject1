package _04_operateur;

public class _05_transtypage {

	/**
	 * Transtypage : transformer un type en un autre type
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Transtypage implicite : on convertit un type plus petit (en mémoire alloué)
		 * en un type plus grand ou un entier vers un nombre à virgule.
		 * => pas de perte de précision
		 */
		byte myByte = 111;
		short myShort = myByte;
		int myInt = myShort;
		long myLong = myInt;
		float myFloat = myLong;
		double myDouble = myFloat;

		System.out.println(myByte);
		System.out.println(myShort);
		System.out.println(myInt);
		System.out.println(myLong);
		System.out.println(myFloat);
		System.out.println(myDouble);
		System.out.println();
		
		/*
		 * Transtypage explicite : on convertit un type plus grand en un type plus petit
		 * ou un nombre à virgule en un entier on a donc un risque de perte de précision
		 * => cast est obligatoire.
		 */
		
		myDouble = 7493982498.498197467;
		myFloat = (float) myDouble;
		myLong = (long) myFloat;
		myInt = (int) myLong;
		myShort = (short) myInt;
		myByte = (byte) myShort;
		System.out.println(myDouble);
		System.out.println(myFloat);
		System.out.println(myLong);
		System.out.println(myInt);
		System.out.println(myShort);
		System.out.println(myByte);
		// il faut que le type qui reçoit est la capacité de contenir la valeur
	}

}
