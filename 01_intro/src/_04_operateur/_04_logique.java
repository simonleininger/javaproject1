package _04_operateur;

public class _04_logique {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean a = false, b = false;
		for (int i = 0; i < 4; i++) {
			System.out.println("A est " + a + " et B est " + b);
			System.out.println("A ET B est " + (a && b));
			System.out.println("A OU B est " + (a || b));
			System.out.println("A XOR B est " + (a ^ b));
			b = !b;
			if (i == 1) {
				a = !a;
			}
			System.out.println();
		}

	}

}
