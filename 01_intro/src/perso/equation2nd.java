package perso;

import java.util.Scanner;
/**
 * 
 * @author Simon
 *
 */
public class equation2nd {
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		
		int a=0;
		int b=0;
		int c=0;
		System.out.println("Entrez a : ");
		a = clavier.nextInt();
		System.out.println("Entrez b : ");
		b = clavier.nextInt();
		System.out.println("Entrez c : ");
		c = clavier.nextInt();
		double delta = b*b-4*a*c;
		if (delta>0) {
			System.out.println("Delta est positif, il y a donc 2 solutions");
			double x1 = (-b - Math.sqrt(delta))/2*a;
			double x2 = (-b + Math.sqrt(delta))/2*a;
			System.out.println("x1 = "+x1+" et x2 = "+x2);
		}
		else {
			System.out.println("Je ne sais pas");
		}
		clavier.close();
	}

}
