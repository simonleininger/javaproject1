package _02_EntreeSortie;

import java.util.Scanner;

/**
 * 
 * @author Simon
 *
 */
public class _02_Scan {
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner clavier = new Scanner(System.in);
		
		int n = 0;
		System.out.println("Entrez un nombre entier : ");
		n = clavier.nextInt();
		System.out.println("Le carré de "+n+" vaut : "+n*n);
		System.out.println("Enter un nombre à virgule (utilisez , et pas .) : ");
		double d = clavier.nextDouble();
		System.out.println("Vous avez saisi : "+d);
		System.out.println("Bonjour, merci de me parler :");
		String p = clavier.nextLine();
		System.out.println("Vous avez bien dit : "+p+" ?");
		
		clavier.close();
	}

}
