package _03_type;

public class _01_RegleNomage {

	public static void main(String[] args) {
		/*
		 * une variable commence par :
		 * - une lettre
		 * - un "_"
		 * - soit le caractère $
		 * - mais pas par un chiffre ou un opérateur
		 */

		int monEntier = 28;
		String _chaine = "ma chaîne de caractère";
		char $carac = 'a';
		
		//double 000mondouble; interdit
		//double +double; interdit
		//Par convensition on utilise une miniscule pour le 1er caractère du nom de variable
		//Camel case : camelCase Convention utilisé en Java
		
		//Pascal case : PascalCase
		//Snake Case : sanke_case
		
		System.out.println(monEntier+_chaine+$carac);
	}

}
