package _03_type;

public class _02_Constante {

	public static void main(String[] args) {
		/*
		 * Les constantes est identifié par le mot clé final
		 * par convention les constantes sont déclaré en MAJUSCULE
		 */
		
		final String MA_CONSTANTE="cette chaîne est constante.";
		//MA_CONSTANTE="cette chaîne est constante. modifié";
		System.out.println(MA_CONSTANTE);

	}

}
