package Exercice;

public class _08_Exercice {

	/**
	 * Suppression d’un élément dans un tableau trié
	 * 
	 * La suppression d’un élément d’un tableau qui contient une liste de données
	 * décale vers la gauche les éléments situés à droite de l’élément à supprimer.
	 * Le premier décalage écrase la valeur à supprimer par le contenu de la case de
	 * droite. Les décalages successifs répètent cette opération sur les cases
	 * suivantes, jusqu’à la fin des données.
	 * 
	 * Créer et initialiser un tableau, puis supprimer un élément de ce tableau à la
	 * position spécifiée (de 0 à N-1). Pour supprimer un élément du tableau,
	 * déplacez les éléments, juste après la position donnée vers une position à
	 * gauche et réduisez la taille du tableau. Exemple :
	 * 
	 * Données d'entrée : Saisir le nombre de notes :
	 * 
	 * Note 1 : 8.5 Note 2 : 9.5 Note 3 : 11 Note 3 : 12.5 Note 4 : 18.0 Saisir la
	 * position de l'élément à supprimer : 2 Données de sortie : [8.5, 11.0, 12.5,
	 * 18.0]
	 * 
	 * @param args
	 */
	public static int[] supEle(int[] list, int index) {
		int[] newtab = new int[list.length - 1];
		for (int i = 0; i < newtab.length; i++) {
			if (i < index) {
				newtab[i] = list[i];
			}
			if (i >= index) {
				newtab[i] = list[i+1];
			}
		}
		return newtab;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] tab = { 8, 9, 11, 12, 18 };
		int[] nouveau = supEle(tab, 2);
		for (int i = 0; i < nouveau.length; i++) {
			System.out.println(nouveau[i]);
		}

	}

}
