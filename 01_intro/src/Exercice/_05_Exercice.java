package Exercice;

import java.util.Scanner;

public class _05_Exercice {

	/**
	 * Vérifier si l'année donnée par l'utilisateur est bissextile (366 jours) ou non.
	 * Une année est considérée comme une année bissextile si :
	 * elle est divisible par 4 et non divisible par 100 ;
	 * ou si elle est divisible par 400
	 * « divisible » signifie que la division donne un nombre entier, 
	 * sans reste (c'est à dire le reste égale à zéro) : 21 est divisible par 3. 22 non.
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Ecrivez une année : ");
		int nb = clavier.nextInt();
		if ((nb % 4 == 0 && nb % 100 != 0) || nb % 400 == 0) {
			System.out.println(nb + " est une année bissextile");
		} else {
			System.out.println(nb + " n'est pas une année bissextile");
		}
		clavier.close();
	}

}
