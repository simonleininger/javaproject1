package Exercice;

public class _01_Exercice {

	/**
	 * Permute la valeur de 2 nombres
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 5;
		int b = 9;
		System.out.println("Avant permutation : a = " + a + " et b = " + b);
		int temp = a;
		a = b;
		b = temp;
		System.out.println("Après permutation : a = " + a + " et b = " + b);
	}

}
