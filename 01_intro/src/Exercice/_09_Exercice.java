package Exercice;

import java.util.Scanner;

public class _09_Exercice {

	/**
	 * Le problème est de déterminer si un nombre entier est un nombre premier.
	 * Définition : Un nombre premier est un nombre : - strictement supérieur à 1 -
	 * qui n’est divisible (au sens de la division entière) que par 1 et par
	 * lui-même.
	 * 
	 * Ainsi : 2 est un nombre premier (il est uniquement divisible par 1 et par
	 * lui-même) 3 aussi 4 n’est pas premier (il est divisible par 2) 5 est premier
	 * etc...
	 * 
	 * Le nombre 1 pourrait être considéré comme un nombre premier (il n’est pas
	 * divisible sauf par 1 et par lui-même). Il est exclu de la liste des nombres
	 * premiers par commodité, car il ne respecte pas certaines lois communes à tous
	 * les autres nombres premiers. La méthode de base consiste à prouver que le
	 * nombre à traiter n’est pas premier en recherchant un diviseur qui donne un
	 * reste égal à 0. Si on en trouve un, le nombre n’est pas premier. Si aucun
	 * diviseur n’est trouvé, il s’agit d’un nombre premier.
	 * 
	 * Écrivez un premier programme qui effectue ce calcul. On utilisera une boucle
	 * FOR afin de tester tous les diviseurs de 2 au nombre – 1. Le programme
	 * demandera un nombre entier et indiquera s’il est premier ou non.
	 * 
	 * @param args
	 */
	public static boolean isPremier(int nb) {
		for (int i = 2; i < nb; i++) {
			if (nb % i == 0) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Entrez un nombre entier positif");
		int nb = clavier.nextInt();
		if (nb == 1 || nb == 2) {
			System.out.println(nb + " est un nombre premier");
		} else {
			if (isPremier(nb)) {
				System.out.println(nb + " est un nombre premier");
			} else {
				System.out.println(nb + " n'est pas un nombre premier");
			}
		}
		clavier.close();
		System.out.print("1, ");
		for (int i = 2; i < 1000; i++) {
			if (isPremier(i)) {
				System.out.print(i + ", ");
			} 
		}
	}
}
