package Exercice;

import java.util.Scanner;

public class _03_Exercice {

	/**
	 * Indique si un nombre est pair ou impair
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Ecrivez un nombre entier : ");
		int nb = clavier.nextInt();
		if (nb % 2 == 0) {
			System.out.println(nb + " est un nombre pair");
		} else {
			System.out.println(nb + " est un nombre impair");
		}
		clavier.close(); 
	}

}
