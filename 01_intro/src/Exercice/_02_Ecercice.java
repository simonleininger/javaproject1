package Exercice;

import java.util.Scanner;

public class _02_Ecercice {

	/**
	 * Donne la racine carré d'un nombre
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Ecrivez un nombre entier : ");
		int nb = clavier.nextInt();
		System.out.println("Vous avez saisi " + nb + " sa racine carré est : " + (Math.sqrt(nb)));
		clavier.close();
	}

}
