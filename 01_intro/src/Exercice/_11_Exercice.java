package Exercice;

import java.util.Scanner;

public class _11_Exercice {

	/**
	 * PGCD : Plus Grand Dénominateur Commun
	 *
	 * Appliquer l’Algorithme d’Euclide qui dit : si b divise a alors pgcd(a,b) = b
	 * sinon pgcd(a,b) = pgcd(b, a mod b)
	 * 
	 * où a mod b est le reste de la division de a par b. Ecrivez un programme
	 * permettant de trouver le plus PGCD à partir cet algorithme.
	 * 
	 * @param args
	 */
	public static int pgcd(int num, int denom) {
		if (denom == 0) {
			return num;
		}
		return pgcd(denom, num % denom);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		System.out.println("Nous allons cherche le plus grand dénominateur commun pour a/b");
		System.out.println("Merci de saisir a :");
		int a = clavier.nextInt();
		System.out.println("Merci de saisir b :");
		int b = clavier.nextInt();
		int pg = pgcd(a, b);
		System.out.println("Le PGCD de " + a + " et " + b + " est : " + pg);
		System.out.println("Donc " + a + " / " + b + " est = " + (a/pg) + " / " + (b/pg));
		clavier.close();
	}

}
