package Exercice;

import java.util.Scanner;

public class _06_Execice {

	/**
	 * L'utilisateur doit deviner un nombre secret (généré automatiquement) entre 1
	 * et 1000. On va donc demander à l'utilisateur de trouver ce nombre secret.
	 * Tant qu'il n'a pas trouvé ce nombre on lui demandera encore et encore,
	 * jusqu'à ce qu'il le trouve. Si l'utilisateur choisit un nombre trop petit,
	 * l'application lui dira que le nombre qu’il a rentré est trop petit Si
	 * l'utilisateur choisit un nombre trop grand, l'application lui dira que le
	 * nombre qu’il a rentré est trop grand Si l'utilisateur trouve le nombre
	 * recherché, l'application lui indiquera le nombre d'essais dont il a eu besoin
	 * 
	 * Exemple : Entrée : Entrer un nombre entre 1 et 1000 (1 - 1000) : 500 Sorties
	 * possibles : Votre nombre est trop grand ! Votre nombre est trop petit !
	 * Trouvé en 8 essais ! Bien Joué !
	 *
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner clavier = new Scanner(System.in);
		int rand = (int) (1000 * Math.random());
		System.out.println("Trouver le nombre : ");
		int a = clavier.nextInt();
		int i = 1;
		do {
			if (a < rand) {
				System.out.println("Le nombre recherché est plus grand, encore " + (10 - i) + " tentative(s)");
				i++;
				a = clavier.nextInt();
			}
			if (a > rand) {
				System.out.println("Le nombre recherché est plus petit, encore " + (10 - i) + " tentative(s)");
				i++;
				a = clavier.nextInt();
			}
		} while (a != rand && i != 10);
		if (a == rand) {
			System.out.println("Vous avez trouvé : " + rand + " au bout de " + i + " tentatives");
		} else {
			System.out.println("GAME OVER!!!\nYOU'RE A LOSER\nIl fallait trouver : " + rand);
		}
		clavier.close();
	}

}
