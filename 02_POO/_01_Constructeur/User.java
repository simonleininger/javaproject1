package _01_Constructeur;

/**
 * 
 * @author Simon
 *
 */
public class User {
	// Variable d'instance, chaque user � son propre nom, pr�nom et age
	public String nom;
	public String prenom;
	public int age;
	public int id;

	// le mot cl� static permet de d�finir des variable de Classe (par opposition au
	// variable d'instance)
	// Varible de classe elle est commune � toutes les instances.
	public static int nUser;

	/*
	 * un constructeur est une m�thode sp�ciale : - qui porte le m�me nom que la
	 * classe - qui ne retourne rien - qui sert � instancier ( cr�er des instances
	 * de la classe)
	 */
	public User() {
		nUser++;
		id = nUser;

	}

	public User(String prenom, String nom) {
		this();
		this.nom = nom;
		this.prenom = prenom;
	}

	public User(String prenom, String nom, int age) {
		this(prenom, nom); // appel du constructeur � 2 param�tres
		this.age = age;
	}

	@Override
	public String toString() {
		if (age != 0) {
			return "User " + id + " [pr�nom = " + prenom + ", nom = " + nom + ",  age = " + age + " ans]";
		} else {
			return "User " + id + " [pr�nom = " + prenom + ", nom = " + nom + "]";
		}
	}

	/**
	 * SUPER() Appel au constructeur de la "super classe" (ou classe m�re) en Java
	 * toute les classes h�ritent implicitement de la classe "object" comme notre
	 * classe "User" n'h�rite pas d'autre classe, sa super classe est donc la classe
	 * "Object"
	 * 
	 * @param prenom
	 */
}
