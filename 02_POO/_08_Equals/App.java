package _08_Equals;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Compte_Bancaire cb1 = new Compte_Bancaire("Doudou", "28102022", 500.45);
		Compte_Bancaire cb2 = new Compte_Bancaire("Baba", "29092018", 540.45);
		Compte_Bancaire cb3 = new Compte_Bancaire("Doudou", "28102022", 500.45);
		Compte_Bancaire cb4 = new Compte_Bancaire("Frankie", "09051994", 5040.45);
		System.out.println(cb1);
		System.out.println(cb2);
		System.out.println(cb3);
		System.out.println(cb4);

		cb1.equals(cb2);

		System.out.println("Hashcode cb1 : " + cb1.hashCode());
		System.out.println("Hashcode cb2 : " + cb2.hashCode());
		System.out.println("Hashcode cb3 : " + cb3.hashCode());
		System.out.println("Hashcode cb4 : " + cb4.hashCode());
		System.out.println("Cb1 == Cb2 ? " + cb1.equals(cb2));
		System.out.println("Cb1 == Cb3 ? " + cb1.equals(cb3));
		System.out.println("Cb1 == Cb4 ? " + cb1.equals(cb4));
	}

}
