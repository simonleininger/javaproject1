package _08_Equals;

import java.util.Objects;

public class Compte_Bancaire {
	private String client;
	private String numCompte;
	private double solde;

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public Compte_Bancaire() {
		super();

	}

	public Compte_Bancaire(String client, String numCompte, double solde) {
		this();
		this.client = client;
		this.numCompte = numCompte;
		this.solde = solde;
	}

	@Override
	public String toString() {
		return "Compte_Bancaire [numCompte = " + numCompte + ", client = " + client + ", solde = " + solde + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(client, numCompte, solde);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compte_Bancaire other = (Compte_Bancaire) obj;
		return Objects.equals(client, other.client) && Objects.equals(numCompte, other.numCompte)
				&& Double.doubleToLongBits(solde) == Double.doubleToLongBits(other.solde);
	}

}
