package _04_Heritage;

/**
 * extends User
 * 
 * -permet � la classe Client d'h�riter des param�tres et m�thode de la classe
 * User -super(param�tre) permet d'appeler le constructeur de la classe User
 * 
 * @author Simon
 *
 */
public class Client extends User {
	private String phone;
	private boolean sub;

	private static int nClient;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		if (this.phone == null)
		{
			nClient++;
		}
		this.phone = phone;
		
	}

	public boolean isSub() {
		return sub;
	}

	public void setSub(boolean sub) {
		this.sub = sub;
	}

	public static int getnClient() {
		return nClient;
	}

	public Client() {
		super();
		nClient++;
	}

	public Client(String phone) {
		this();
		this.phone = phone;
	}

	public Client(String phone, boolean sub) {
		this(phone);
		this.sub = sub;
	}

	public Client(String prenom, String nom) {
		super(prenom, nom);
	}

	public Client(String prenom, String nom, int age) {
		super(prenom, nom, age);
	}

	public Client(String prenom, String nom, int age, String phone) {
		super(prenom, nom, age);
		this.phone = phone;
		nClient++;
	}

	@Override
	public String toString() {
		return "Client [phone=" + phone + ", sub=" + sub + ", " + super.toString() + "]";
	}

}
