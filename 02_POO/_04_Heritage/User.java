package _04_Heritage;

/**
 * 
 * @author Simon
 *
 */
public class User {
	// Variable d'instance
	private String nom;
	private String prenom;
	private int age;
	private int id;

	// Variable de Classe
	private static int nUser;

	// M�thode Getters et Setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		if (age <= 0) {
			System.out.println("Un age ne peut pas �tre n�gatif");
		} else {
			this.age = age;
		}
	}

	public int getId() {
		return id;
	}

	public static int getnUser() {
		return nUser;
	}

	// Constructeurs
	public User() {
		nUser++;
		id = nUser;
	}

	public User(String prenom, String nom) {
		this();
		this.nom = nom;
		this.prenom = prenom;
	}

	public User(String prenom, String nom, int age) {
		this(prenom, nom);
		this.age = age;
	}

	@Override
	public String toString() {
		if (age != 0) {
			return "User " + id + " [pr�nom = " + prenom + ", nom = " + nom + ",  age = " + age + " ans]";
		} else {
			return "User " + id + " [pr�nom = " + prenom + ", nom = " + nom + "]";
		}
	}
}