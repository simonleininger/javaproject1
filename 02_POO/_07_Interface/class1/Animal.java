package _07_Interface.class1;

/**
 * Une interface est un contrat que toute classe qui impl�mente cette interface
 * devra respecter. En java une interface n'est pas un classe mais un ensemble
 * d'exigence ou fonctionnalit� pour les classes qui souhaite s'y conformer.
 * 
 * Une interface se pr�sente sous la forme d'un ensemble de m�thode abtraite.
 * Contrairement � une classe, une interface ne peut pas avoir de constructeurs et d'attributs.
 * Une interface ne peut compter que des champs static ou d�finit par le mot cl� final.
 * 
 * @author Simon
 *
 */
public interface Animal {
	
	final String hab = "Terrestre";
	void communiquer();
}
