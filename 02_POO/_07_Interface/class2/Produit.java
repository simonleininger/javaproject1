package _07_Interface.class2;

public class Produit {
	private int id;
	private String description;
	private double prix;

	private static int nProduit;

	/******************************************************
	 * Getters Setters
	 ******************************************************/
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getId() {
		return id;
	}

	public static int getnProduit() {
		return nProduit;
	}

	/******************************************************
	 * Constructeurs
	 ******************************************************/
	public Produit() {
		super();
		nProduit++;
		this.id = nProduit;
	}

	public Produit(String description, double prix) {
		this();
		this.description = description;
		this.prix = prix;
	}

	/******************************************************
	 * M�thodes
	 ******************************************************/
	@Override
	public String toString() {
		return "Produit [id=" + id + ", description=" + description + ", prix=" + prix + "]";
	}

}
