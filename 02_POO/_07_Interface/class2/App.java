package _07_Interface.class2;

public class App {

	public static void main(String[] args) {

		ProduitRepository rMS = new ProduitMySqlRepositoryImpl();
		ProduitRepository rSS = new ProduitSqlServerRepositoryImpl();
		Produit pMS = new Produit("25 Couches Pampers", 24.99);
		Produit pSS = new Produit("Lait en poudre 500g", 9.99);
		System.out.println(pMS+" "+pSS);
		rSS.updateProduct(pSS);
		rMS.deleteProduct(pMS);
		System.out.println(pMS+" "+pSS);
	}

}
