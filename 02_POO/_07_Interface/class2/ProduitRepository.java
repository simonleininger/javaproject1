package _07_Interface.class2;

import java.util.List;

public interface ProduitRepository {

	List<Produit> getAll();

	void addProduct(Produit p);

	void updateProduct(Produit p);

	void deleteProduct(Produit p);
}