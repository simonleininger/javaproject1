package _09_Generecite;

import java.util.Objects;

/**
 * Le concepte de g�n�r�cit� peut s'appliqu� � des classes, objets, m�thodes,
 * interface identiques sur le plan algorithemique Mais qui manipule des types
 * de donn�e diff�rent l'int�r�t c'est l'optimisation du code (moins de cast
 * transtypage) et �a limite le nombre d'erreur de l'application
 * 
 * @author Simon
 *
 */
public class Generic<T> {
	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public Generic() {
		super();
	}

	public Generic(T data) {
		this();
		this.data = data;
	}

	@Override
	public String toString() {
		return "Generic data = " + data;
	}

	@Override
	public int hashCode() {
		return Objects.hash(data);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Generic<?> other = (Generic<?>) obj;
		return Objects.equals(data, other.data);
	}

	public boolean Myequals(Generic<?> g) {

		return data.equals(g.data);
	}

}
