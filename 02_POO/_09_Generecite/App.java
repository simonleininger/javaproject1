package _09_Generecite;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Generic<?>[] g = new Generic<?>[11];
		g[0] = new Generic<>(2022);
		g[1] = new Generic<>(10);
		g[2] = new Generic<>(28);
		g[3] = new Generic<>(28.10);
		g[4] = new Generic<>(9.05);
		g[5] = new Generic<>("Doudou");
		g[6] = new Generic<>("Turlutte");
		g[7] = new Generic<>(28);
		g[8] = new Generic<>(Math.PI);
		g[9] = new Generic<>(19);
		g[10] = new Generic<>("Turlutte");

		for (int i = 0; i < g.length; i++) {
			System.out.println(g[i]);
			for (int j = i+1; j < g.length; j++) {
				System.out.println("Est ce qu\'il est �gale � " + g[j] + " : " + g[i].Myequals(g[j]));
			}
			System.out.println("*********************************************");
		}
	}

}