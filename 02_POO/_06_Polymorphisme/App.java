package _06_Polymorphisme;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class App {

	public static void main(String[] args) throws InterruptedException {
		List<Animal> animaux = new ArrayList<Animal>();
		for (int i = 0; i < 500; i++) {
			if (i % 5 == 0) {
				animaux.add(new Chat());
			} else {
				animaux.add(new Chien());
			}
		}
		for (Animal ani : animaux) {
			ani.communiquer(); // 2 animaux peuvent avoir des comportement différents => Polymorphisme
			TimeUnit.MILLISECONDS.sleep(10);
		}

	}

}
