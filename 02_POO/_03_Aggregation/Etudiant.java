package _03_Aggregation;

public class Etudiant {
	private String prenom;
	private String nom;
	private int id;
	private Notes notes;

	private static int nEtudiant;

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getId() {
		return id;
	}

	public Notes getNotes() {
		return notes;
	}

	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	public static int getnEtudiant() {
		return nEtudiant;
	}

	public Etudiant() {
		super();
		nEtudiant++;
		id = nEtudiant;
	}

	public Etudiant(String prenom, String nom) {
		this();
		this.prenom = prenom;
		this.nom = nom;
	}

	public Etudiant(String prenom, String nom, Notes notes) {
		this(prenom, nom);
		this.notes = notes;
	}

	@Override
	public String toString() {
		return "Etudiant Pr�nom = " + prenom + "\n\t\t  Nom = " + nom + "\n\t\t  " + notes;
	}

}
