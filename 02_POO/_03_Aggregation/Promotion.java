package _03_Aggregation;

import java.util.List;

public class Promotion {
	private List<Etudiant> etudiants;
	private int annee;

	public Promotion(List<Etudiant> etudiants, int annee) {
		super();
		this.etudiants = etudiants;
		this.annee = annee;
	}

	@Override
	public String toString() {
		String result = "********************************************************************";
		result += "\nPromotion " + annee + " : \n\n";
		for (Etudiant etudiant : etudiants) {
			result += "\t-" + etudiant + "\n\n";
		}
		return result;
	}

}
