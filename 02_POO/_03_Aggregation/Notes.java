package _03_Aggregation;

public class Notes {
	private int maths;
	private int geo;

	public int getMaths() {
		return maths;
	}

	public void setMaths(int maths) {
		this.maths = maths;
	}

	public int getGeo() {
		return geo;
	}

	public void setGeo(int geo) {
		this.geo = geo;
	}

	public Notes(int maths, int geo) {
		super();
		this.maths = maths;
		this.geo = geo;
	}

	@Override
	public String toString() {
		return "Notes : [maths=" + maths + ", geo=" + geo + "]";
	}

}
