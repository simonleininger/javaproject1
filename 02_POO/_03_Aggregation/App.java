package _03_Aggregation;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		Notes notes = new Notes(15, 16);
		Etudiant e1 = new Etudiant("Jean-Philippe", "Martin", notes);
		Etudiant e2 = new Etudiant("Fran�ois-Xavier", "De Coubertin", new Notes(12, 7));
		Etudiant e3 = new Etudiant("Gilles-Henri", "Villard", new Notes(2, 4));
		Etudiant e4 = new Etudiant("Am�lia", "Leininger", new Notes(15, 14));
		Etudiant e5 = new Etudiant("Lisa", "Mark", new Notes(5, 5));

		List<Etudiant> liste = new ArrayList<Etudiant>();
		List<Etudiant> liste1 = new ArrayList<Etudiant>();
		liste.add(e1);
		liste.add(e2);
		liste.add(e3);
		liste1.add(e4);
		liste1.add(e5);

		Promotion p1 = new Promotion(liste,2022);
		Promotion p2 = new Promotion(liste1,2023);

		System.out.println(p1);
		System.out.println(p2);
	}

}
