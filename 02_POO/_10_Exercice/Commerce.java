package _10_Exercice;

/**
 * N'h�sitez pas � introduire des classes interm�diaires pour �viter au maximum
 * les redondances d'attributs et de m�thodes dans les sous-classes
 * 
 * Commerce permet de prendre le Chiffre d'affaire de chaque employ� de type
 * Vente et Representation ainsi que le taux de ce chiffre d'affaire qu'il
 * auront sur leur salaire.
 * 
 * @author Simon
 *
 */
public class Commerce extends Employe {

	public final int tauxCA = 20;
	public int ca;

	@Override
	public double calculerSalaire() {
		return ca * tauxCA / 100;
	}

	public Commerce() {
		super();
	}

	public Commerce(String prenom, String nom, int age, String dateEntree, int ca) {
		super(prenom, nom, age, dateEntree);
		this.ca = ca;
	}

}
