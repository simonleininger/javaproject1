package _10_Exercice;

import java.util.ArrayList;

/**
 * Collection d'employ�s
 * 
 * Satisfait de la hi�rarchie propos�e, notre directeur souhaite maintenant
 * l'exploiter pour afficher le salaire de tous ses employ�s ainsi que le
 * salaire moyen. Ajoutez une classe Personnel contenant une "collection"
 * d'employ�s. Il s'agira d'une collection polymorphique d'Employe - regardez le
 * cours si vous ne voyez pas de quoi il s'agit.
 * 
 * @author Simon
 *
 */
public class Personnel {

	public ArrayList<Employe> listEmp = new ArrayList<Employe>();

	// void calculerSalaires() : affiche le salaire de chacun des employ�s de la
	// collection.
	public void calculerSalaires() {
		for (int i = 0; i < listEmp.size(); i++) {
			System.out.print(listEmp.get(i).getName() + " � un Salaire de : ");
			System.out.println(listEmp.get(i).calculerSalaire() + " �.");
		}
	}

	// void ajouterEmploye(Employe) : ajoute un employ� � la collection.
	void ajouterEmploye(Employe e) {
		listEmp.add(e);
	}

	// double salaireMoyen() : affiche le salaire moyen des employ�s de la
	// collection.
	public double salaireMoyen() {
		double moySalaire = 0;
		for (int i = 0; i < listEmp.size(); i++) {
			moySalaire += listEmp.get(i).calculerSalaire();
		}
		moySalaire = Math.round(moySalaire / listEmp.size());
		return moySalaire;
	}

	public Personnel() {
		super();
	}

	public void afficherEmployes() {
		System.out.println(this);
	}

	@Override
	public String toString() {
		return "Personnel Liste des Employ�s = \n" + listEmp;
	}

}
