package _10_Exercice;

/**
 * Ceux affect�s � la Production. Leur salaire vaut le nombre d'unit�s produites
 * mensuellement multipli�es par 5.
 * 
 * @author Simon
 * 
 */
public class Production extends EmpRisque {
	public final int multiProd = 5;
	public int nbProd;

	public String getName() {
		return "Le Technicien " + getPrenom() + " " + getNom();
	}

	@Override
	public double calculerSalaire() {
		return nbProd * multiProd + super.calculerSalaire();
	}
	
	public Production() {
		super();
	}

	public Production(String prenom, String nom, int age, String dateEntree, int nbProd, boolean droitPrime) {
		super(prenom, nom, age, dateEntree, droitPrime);
		this.nbProd = nbProd;
	}
}
