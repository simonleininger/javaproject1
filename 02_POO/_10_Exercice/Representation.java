package _10_Exercice;

/**
 * Ceux affect�s � la Repr�sentation. Leur salaire mensuel est �galement de 20 %
 * du chiffre d'affaire qu'ils r�alisent mensuellement, plus 800 Euros.
 *  
 * @author Simon
 *
 */
public class Representation extends Commerce {
	public final int salaireFixe = 800;

	public String getName() {
		return "Le Repr�sentant " + getPrenom() + " " + getNom();
	}

	@Override
	public double calculerSalaire() {
		return super.calculerSalaire() + salaireFixe;
	}

	public Representation() {
		super();
	}

	public Representation(String prenom, String nom, int age, String dateEntree, int ca) {
		super(prenom, nom, age, dateEntree, ca);
	}
}
