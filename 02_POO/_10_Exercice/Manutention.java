package _10_Exercice;

/**
 * Ceux affect�s � la Manutention. Leur salaire vaut leur nombre d'heures de
 * travail mensuel multipli�es par 65 Euros.
 * 
 * @author Simon
 * 
 */
public class Manutention extends EmpRisque {
	public final int multiHeure = 65;
	public int nbHeure;

	public String getName() {
		return "Le Manutentionnaire " + getPrenom() + " " + getNom();
	}

	@Override
	public double calculerSalaire() {
		return nbHeure * multiHeure + super.calculerSalaire();
	}

	public Manutention() {
		super();
	}

	public Manutention(String prenom, String nom, int age, String dateEntree, int nbHeure, boolean droitPrime) {
		super(prenom, nom, age, dateEntree, droitPrime);
		this.nbHeure = nbHeure;
	}
}
