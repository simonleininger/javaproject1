package _10_Exercice;

/**
 * Employ�s � risques
 * 
 * Certains employ�s des secteurs production et manutention sont appel�s �
 * fabriquer et manipuler des produits dangereux. Apr�s plusieurs n�gociations
 * syndicales, ces derniers parviennent � obtenir une prime de risque mensuelle.
 * Compl�tez votre programme java en introduisant deux nouvelles sous-classes
 * d'employ�s.
 * 
 * Ces sous-classes d�signeront les employ�s des secteurs production et
 * manutention travaillant avec des produits dangereux.
 * 
 * Modifier votre programme de sorte � attribuer une prime mensuelle de 200 �
 * aux employ�s � risque.
 * 
 * @author Simon
 *
 */
public class EmpRisque extends Employe {
	public final int prime = 200;
	public boolean droitPrime;

	@Override
	public double calculerSalaire() {
		if (droitPrime) {
			return prime;
		} else {
			return 0;
		}
	}

	public EmpRisque() {
		super();
	}

	public EmpRisque(String prenom, String nom, int age, String dateEntree, boolean droitPrime) {
		super(prenom, nom, age, dateEntree);
		this.droitPrime = droitPrime;
	}
}
