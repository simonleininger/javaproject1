package _10_Exercice;

/**
 * Ceux affect�s � la Vente. Leur salaire mensuel est le 20 % du chiffre
 * d'affaire qu'ils r�alisent mensuellement, plus 400 Euros.
 * 
 * @author Simon
 * 
 */
public class Vente extends Commerce {
	public final int salaireFixe = 400;

	public String getName() {
		return "Le Vendeur " + getPrenom() + " " + getNom();
	}

	@Override
	public double calculerSalaire() {
		return super.calculerSalaire() + salaireFixe;
	}

	public Vente() {
		super();
	}

	public Vente(String prenom, String nom, int age, String dateEntree, int ca) {
		super(prenom, nom, age, dateEntree, ca);
	}
}
