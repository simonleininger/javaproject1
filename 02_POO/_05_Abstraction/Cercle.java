package _05_Abstraction;

public class Cercle extends Forme {

	private int rayon;

	public int getRayon() {
		return rayon;
	}

	public void setRayon(int rayon) {
		if (rayon <= 0) {
			throw new IllegalArgumentException("le rayon doit �tre strictement positif");
		}
		this.rayon = rayon;
	}

	public Cercle(int rayon) {
		super();
		this.rayon = rayon;
	}

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return Math.round(rayon * rayon * Math.PI * 100) / 100.0;
	}

	@Override
	public double perimetre() {
		// TODO Auto-generated method stub
		return Math.round(2 * Math.PI * rayon * 100) / 100.0;
	}

	public int diametre() {
		// TODO Auto-generated method stub
		return rayon * 2;
	}

	@Override
	public String toString() {
		return super.toString() + "Un cercle de rayon = " + rayon + " a un diametre = " + diametre()
				+ ", un perimetre = " + perimetre() + ", et une surface = " + surface();
	}

}
