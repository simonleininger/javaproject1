package _05_Abstraction;

public class Carre extends Forme {

	private int cote;

	public int getCote() {
		return cote;
	}

	public void setCote(int cote) {
		if (cote <= 0) {
			throw new IllegalArgumentException("le c�t� du carr� doit �tre strictement positif");
		}
		this.cote = cote;
	}

	public Carre(int cote) {
		super();
		this.cote = cote;
	}

	@Override
	public double surface() {
		// TODO Auto-generated method stub
		return cote * cote;
	}

	@Override
	public double perimetre() {
		// TODO Auto-generated method stub
		return 4 * cote;
	}

	@Override
	public String toString() {
		return super.toString() + "Un carr� de c�t� = " + cote + ", a un perimetre = " + perimetre()
				+ " et une surface = " + surface();
	}

}
