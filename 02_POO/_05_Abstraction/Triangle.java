package _05_Abstraction;

public abstract class Triangle extends Forme {
	private int ab;
	private int bc;
	private int cb;

	public int getAb() {
		return ab;
	}

	public void setAb(int ab) {
		this.ab = ab;
	}

	public int getBc() {
		return bc;
	}

	public void setBc(int bc) {
		this.bc = bc;
	}

	public int getCb() {
		return cb;
	}

	public void setCb(int cb) {
		this.cb = cb;
	}

	public Triangle(int ab, int bc, int cb) {
		super();
		this.ab = ab;
		this.bc = bc;
		this.cb = cb;
	}

}
