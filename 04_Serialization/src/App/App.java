package App;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Helpers.Binary;
import Helpers.XML;
import Model.Adresse;

public class App {

	public static void main(String[] args) {
		Adresse ad1 = new Adresse (6, "rue du pensionnat", 67520, "Marlenheim");
		Adresse ad2 = new Adresse (67000, "Strasbourg");
		Adresse ad3 = new Adresse (13, "rue du moulin", "Wangen", 67520);
		ArrayList<Adresse> list = new ArrayList<Adresse>();
		list.add(ad1);
		list.add(ad2);
		list.add(ad3);
		//test
		try {
			XML.encodeToFile(ad2, "Export/adress.xml");
			Adresse ad4 = (Adresse) XML.decodeFromFile("Export/adress.xml");
			Binary.encodeToFile(list, "Export/adress.bin");
			Object ad5 = Binary.decodeFromFile("Export/adress.bin");
			System.out.println(ad4);
			if (ad5 instanceof ArrayList<?>) {
				ArrayList<?> array = (ArrayList<?>) ad5;
				for (int i=0;i<array.size() ; i++)
				{
					System.out.println(array.get(i));
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
