package Model;

import java.io.Serializable;

public class Adresse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numero;
	private String voie;
	private String ville;

	// Mot cl� transient : le champ sera ignor� dans le fichier XML
	private transient int codePostal = 0;

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public int getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	public Adresse() {
		super();
	}

	public Adresse(int codePostal, String ville) {
		this();
		this.ville = ville;
		this.codePostal = codePostal;
	}

	public Adresse(int numero, String voie, int codePostal, String ville) {
		this(codePostal, ville);
		this.numero = numero;
		this.voie = voie;

	}

	public Adresse(int numero, String voie, String ville, int codePostal) {
		this(codePostal, ville);
		this.numero = numero;
		this.voie = voie;
	}

	@Override
	public String toString() {
		return "Adresse : " + numero + ", " + voie + " " + codePostal + " " + ville;
	}

}
