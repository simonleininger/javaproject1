package _02_Parking;

public class ParkingVideException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ParkingVideException() {
		super("Le parking est d�j� vide. C'est pas possible qu'une voiture parte s'il n'y en a pas");
	}
	
	public ParkingVideException(String message) {
		super(message);
	}
}
