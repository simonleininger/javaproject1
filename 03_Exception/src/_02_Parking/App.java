package _02_Parking;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
	
	private static final Logger log = LogManager.getLogger(App.class);

	public static void main(String[] args) {

		Parking park1 = new Parking(10);
		try {
			park1.remplir(8); //8 occup�s
			park1.depart(); //7
			park1.depart(); //6
			park1.remplir(); //7
			park1.remplir(); //8
			park1.depart(2); //6
			park1.remplir(); //7
			park1.remplir(1); //8
			park1.depart(7); //1
			park1.remplir(); //2
			park1.remplir(5); //7
		} catch (ParkingPleinException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParkingVideException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			log.info(park1);
		}
	}

}
