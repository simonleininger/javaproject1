package _02_Parking;

public class Parking {
	private static int nbPlaceOccupe;
	private int nbPlaceTot;

	public int getNbPlaceTot() {
		return nbPlaceTot;
	}

	public void setNbPlaceTot(int nbPlaceTot) {
		this.nbPlaceTot = nbPlaceTot;
	}

	public int getNbPlaceDispo() {
		return nbPlaceTot - nbPlaceOccupe;
	}

	public void remplir() throws Exception {
		if (nbPlaceTot <= nbPlaceOccupe) {
			throw new ParkingPleinException();
		}
		nbPlaceOccupe++;
		this.parkMessage();
	}

	public void remplir(int plus) throws Exception {
		if (nbPlaceTot <= nbPlaceOccupe + plus) {
			throw new ParkingPleinException();
		}
		nbPlaceOccupe += plus;
		this.parkMessage();
	}

	public void depart() throws Exception {
		if (nbPlaceOccupe == 0) {
			throw new ParkingVideException();
		}
		nbPlaceOccupe--;
		this.parkMessage();
	}

	public void depart(int moins) throws Exception {
		if (nbPlaceOccupe - moins <= 0) {
			throw new ParkingVideException();
		}
		nbPlaceOccupe -= moins;
		this.parkMessage();
	}

	public void parkMessage() {
		System.out.println("Nombre de place dispo : " + this.getNbPlaceDispo());
	}

	public Parking() {
		super();
		nbPlaceOccupe = 0;
	}

	public Parking(int nbPlaceTot) {
		this();
		this.nbPlaceTot = nbPlaceTot;
	}

	@Override
	public String toString() {
		return "Parking : \n\tNombre de Place Total = " + nbPlaceTot + "\n\tNombre de Place Occup�e = " + nbPlaceOccupe
				+ "\n\tNombre de Place Disponible = " + this.getNbPlaceDispo();
	}

}
